# Quote Aggregator - Interview Challenge

version 1.0.0 - 21/04/2021

[![License](https://img.shields.io/badge/license-apache%202.0-blue.svg)](https://opensource.org/licenses/Apache-2.0)
[![Build](https://img.shields.io/bitbucket/pipelines/ivan-sanabria/quote-aggregator.svg)](http://bitbucket.org/ivan-sanabria/quote-aggregator/addon/pipelines/home#!/results/branch/master/page/1)
[![PR](https://img.shields.io/bitbucket/pr/ivan-sanabria/quote-aggregator.svg)](http://bitbucket.org/ivan-sanabria/quote-aggregator/pull-requests)
[![Quality Gate](https://sonarcloud.io/api/project_badges/measure?project=ivan-sanabria_quote-aggregator&metric=alert_status)](https://sonarcloud.io/component_measures/metric/alert_status/list?id=ivan-sanabria_quote-aggregator)
[![Bugs](https://sonarcloud.io/api/project_badges/measure?project=ivan-sanabria_quote-aggregator&metric=bugs)](https://sonarcloud.io/component_measures/metric/bugs/list?id=ivan-sanabria_quote-aggregator)
[![Coverage](https://sonarcloud.io/api/project_badges/measure?project=ivan-sanabria_quote-aggregator&metric=coverage)](https://sonarcloud.io/component_measures/metric/coverage/list?id=ivan-sanabria_quote-aggregator)
[![Size](https://sonarcloud.io/api/project_badges/measure?project=ivan-sanabria_quote-aggregator&metric=ncloc)](https://sonarcloud.io/component_measures/metric/ncloc/list?id=ivan-sanabria_quote-aggregator)
[![TechnicalDebt](https://sonarcloud.io/api/project_badges/measure?project=ivan-sanabria_quote-aggregator&metric=sqale_index)](https://sonarcloud.io/component_measures/metric/sqale_index/list?id=ivan-sanabria_quote-aggregator)
[![Vulnerabilities](https://sonarcloud.io/api/project_badges/measure?project=ivan-sanabria_quote-aggregator&metric=vulnerabilities)](https://sonarcloud.io/component_measures/metric/vulnerabilities/list?id=ivan-sanabria_quote-aggregator)

## Specifications

Please review the README under specifications folder.

## Requirements

- JDK 14.x
- Maven 3.6.3
- IDE for JAVA (Eclipse, Netbeans, IntelliJ).

# Running Application inside IDE

To run the application on your IDE:

1. Verify the version of your JDK - 14.x or higher.
2. Verify the version of Maven - 3.6.3 or higher.
3. Download the source code from repository.
4. Integrate your project with IDE:
    - [Eclipse](http://books.sonatype.com/m2eclipse-book/reference/creating-sect-importing-projects.html)
    - [Netbeans](http://wiki.netbeans.org/MavenBestPractices)
    - [IntelliJ]( https://www.jetbrains.com/idea/help/importing-project-from-maven-model.html)
5. Open IDE and run the class **Application.java**.

## Running Application on Terminal

To run the application on terminal with Maven:

1. Verify the version of your JDK - 14.x or higher.
2. Verify the version of Maven - 3.6.3 or higher.
3. Download the source code from repository.
4. Open a terminal.
5. Go to the root location of the source code.
6. Execute the commands:

```bash
    mvn clean verify
    java -jar target/quote-aggregator-1.0.0.jar
```

## Running Unit Tests on Terminal

To run the unit tests on terminal:

1. Verify the version of your JDK - 14.x or higher.
2. Verify the version of Maven - 3.6.3 or higher.
3. Download the source code from repository.
4. Open a terminal.
5. Go to the root location of the source code.
6. Execute the commands:

```bash
    mvn clean test
```

## Check Application Test Coverage using Jacoco

To verify test coverage on terminal:

1. Verify the version of your JDK - 14.x or higher.
2. Verify the version of Maven - 3.6.3 or higher.
3. Download the source code from repository.
4. Open a terminal.
5. Go to the root location of the source code.
6. Execute the commands:

```bash
    mvn clean verify
    open target/site/jacoco/index.html
```

## List Dependency Licenses

To generate list of dependency licenses on terminal:

1. Verify the version of your JDK - 14.x or higher.
2. Verify the version of Maven - 3.6.3 or higher.
3. Download the source code from repository.
4. Open a terminal.
5. Go to the root location of the source code.
6. Execute the commands:

```bash
    mvn project-info-reports:dependencies
    open target/site/dependencies.html
```
## API Documentation

To see swagger documentation:

1. Open a browser
2. Go to http://${URL}/swagger-ui.html

# Contact Information

Email: icsanabriar@googlemail.com