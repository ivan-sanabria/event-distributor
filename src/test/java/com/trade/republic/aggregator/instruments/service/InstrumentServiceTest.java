/*
 * Copyright (C) 2021 Iván Camilo Sanabria.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.trade.republic.aggregator.instruments.service;

import com.trade.republic.aggregator.instruments.model.Instrument;
import com.trade.republic.aggregator.instruments.repository.InstrumentRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.LinkedList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.BDDMockito.given;

/**
 * Class responsible of handling test cases for the following use cases:
 * <p>
 * - find all instruments in cache    <p>
 * - find instrument by isin in cache <p>
 * - add instrument to cache          <p>
 * - delete instrument from cache     <p>
 *
 * @author Iván Camilo Sanabria (icsanabriar@googlemail.com)
 * @since  1.0.0
 */
@RunWith(SpringRunner.class)
public class InstrumentServiceTest {

    /**
     * Mock of instrument repository.
     */
    @MockBean
    private InstrumentRepository instrumentRepository;


    /**
     * Define instrument repository to simulate operations against cache.
     */
    @MockBean
    InstrumentService instrumentService;

    /**
     * Init the necessary mocks before running test cases.
     */
    @Before
    public void initBean() {
        instrumentService = new InstrumentService(instrumentRepository);
    }


    @Test
    public void given_instruments_retrieves_all_from_cache() {

        final List<Instrument> mockInstruments = new LinkedList<>();
        mockInstruments.add(new Instrument("d1", "isin-1", null));
        mockInstruments.add(new Instrument("d2", "isin-2", null));
        mockInstruments.add(new Instrument("d3", "isin-3", null));

        given(instrumentRepository.getInstruments())
                .willReturn(mockInstruments);

        final List<Instrument> results = instrumentService.getInstruments();
        assertEquals(3, results.size());
    }

    @Test
    public void given_instrument_retrieves_by_isin_from_cache() {

        final String givenIsin = "isin-1";
        final Instrument expectedInstrument = new Instrument("d1", givenIsin, null);

        given(instrumentRepository.getInstrument(givenIsin))
                .willReturn(expectedInstrument);

        final Instrument result = instrumentService.getInstrument(givenIsin);
        assertEquals(expectedInstrument, result);
    }

    @Test
    public void given_instrument_add_to_cache() {

        final Instrument givenInstrument = new Instrument("d1", "isin-1", null);

        given(instrumentRepository.addInstrument(givenInstrument))
                .willReturn(givenInstrument);

        final Instrument result = instrumentService.addInstrument(givenInstrument);
        assertEquals(givenInstrument, result);
    }

    @Test
    public void given_instrument_delete_from_cache() {

        final String givenIsin = "isin-1";
        final Instrument givenInstrument = new Instrument("d1", givenIsin, null);

        given(instrumentRepository.deleteInstrument(givenIsin))
                .willReturn(givenInstrument);

        final Instrument result = instrumentService.deleteInstrument(givenIsin);
        assertEquals(givenInstrument, result);
    }

}
