/*
 * Copyright (C) 2021 Iván Camilo Sanabria.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.trade.republic.aggregator.instruments.repository;

import com.trade.republic.aggregator.instruments.model.Instrument;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.atMost;
import static org.mockito.Mockito.verify;

/**
 * Class responsible of handling test cases for the following use cases:
 * <p>
 * - find all instruments in cache    <p>
 * - find instrument by isin in cache <p>
 * - add instrument to cache          <p>
 * - delete instrument from cache     <p>
 *
 * @author Iván Camilo Sanabria (icsanabriar@googlemail.com)
 * @since  1.0.0
 */
@RunWith(SpringRunner.class)
public class InstrumentRepositoryTest {

    /**
     * Define local repository to spy operations.
     */
    @SpyBean
    private LocalRepository localRepository;

    /**
     * Define instrument repository to simulate operations against cache.
     */
    @Autowired
    private InstrumentRepository instrumentRepository;


    @Test
    public void given_invalid_isin_is_not_added_to_cache() {

        final Instrument i1 = new Instrument("d1", "", null);
        instrumentRepository.addInstrument(i1);

        final Instrument i2 = new Instrument("d2", "isin-2", null);
        instrumentRepository.addInstrument(i2);

        final Instrument i3 = new Instrument("d2", null, null);
        instrumentRepository.addInstrument(i3);

        verify(localRepository, atMost(1)).getInstruments();
    }

    @Test
    public void given_instruments_retrieves_all_from_cache() {

        final Instrument i1 = new Instrument("d1", "isin-1", null);
        instrumentRepository.addInstrument(i1);

        final Instrument i2 = new Instrument("d2", "isin-2", null);
        instrumentRepository.addInstrument(i2);

        final List<Instrument> results = instrumentRepository.getInstruments();
        assertEquals(2, results.size());
    }

    @Test
    public void given_null_isin_retrieves_null_from_cache() {

        final Instrument i1 = new Instrument("d1", "isin-1", null);
        instrumentRepository.addInstrument(i1);

        verify(localRepository, atMost(1)).getInstruments();

        final Instrument result = instrumentRepository.getInstrument(null);
        assertNull(result);
    }

    @Test
    public void given_invalid_isin_retrieves_null_from_cache() {

        final String givenIsin = "";

        final Instrument i1 = new Instrument("d1", "isin-1", null);
        instrumentRepository.addInstrument(i1);

        verify(localRepository, atMost(1)).getInstruments();

        final Instrument result = instrumentRepository.getInstrument(givenIsin);
        assertNull(result);
    }

    @Test
    public void given_isin_retrieves_instrument_from_cache() {

        final String givenIsin = "isin-1";

        final Instrument expected = new Instrument("d1", givenIsin, null);
        instrumentRepository.addInstrument(expected);

        verify(localRepository, atMost(1)).getInstruments();

        final Instrument result = instrumentRepository.getInstrument(givenIsin);
        assertEquals(expected, result);
    }

    @Test
    public void given_null_isin_on_delete_operation_cache_keeps_element() {

        final Instrument expected = new Instrument("d1", "isin-1", null);
        instrumentRepository.addInstrument(expected);

        verify(localRepository, atMost(1)).getInstruments();

        instrumentRepository.deleteInstrument(null);
        verify(localRepository, atMost(1)).getInstruments();
    }

    @Test
    public void given_invalid_isin_on_delete_operation_cache_keeps_element() {

        final String givenIsin = "";

        final Instrument expected = new Instrument("d1", "isin-1", null);
        instrumentRepository.addInstrument(expected);

        verify(localRepository, atMost(1)).getInstruments();

        instrumentRepository.deleteInstrument(givenIsin);
        verify(localRepository, atMost(1)).getInstruments();
    }

    @Test
    public void given_isin_on_delete_operation_cache_is_empty() {

        final String givenIsin = "isin-1";

        final Instrument expected = new Instrument("d1", givenIsin, null);
        instrumentRepository.addInstrument(expected);

        verify(localRepository, atMost(1)).getInstruments();

        instrumentRepository.deleteInstrument(givenIsin);
        verify(localRepository, atMost(0)).getInstruments();
    }

}
