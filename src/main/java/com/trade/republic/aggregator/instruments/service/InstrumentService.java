/*
 * Copyright (C) 2021 Iván Camilo Sanabria.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.trade.republic.aggregator.instruments.service;

import com.trade.republic.aggregator.instruments.model.Instrument;
import com.trade.republic.aggregator.instruments.repository.InstrumentRepository;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Class responsible of handling persistence against local or redis cache for the following use cases:
 * <p>
 *  - find all instruments in cache    <p>
 *  - find instrument by isin in cache <p>
 *  - add instrument to cache          <p>
 *  - delete instrument from cache     <p>
 *
 * @author Iván Camilo Sanabria (icsanabriar@googlemail.com)
 * @since  1.0.0
 */
@Service
public class InstrumentService {

    /**
     * Define repository to inject to store instrument values in local or redis cache.
     */
    private final InstrumentRepository instrumentRepository;

    /**
     * Constructor of the instrument service with given repository.
     *
     * @param instrumentRepository Instrument repository to persist values in cache.
     */
    public InstrumentService(InstrumentRepository instrumentRepository) {
        this.instrumentRepository = instrumentRepository;
    }

    /**
     * Retrieves all instruments from cache.
     *
     * @return All Instruments found in the cache.
     */
    @Cacheable(value = "instruments")
    public List<Instrument> getInstruments() {
        return instrumentRepository.getInstruments();
    }

    /**
     * Retrieves an instrument by isin from cache.
     *
     * @param isin Identifier of the instrument.
     * @return Instrument found in the cache.
     */
    @Cacheable(value = "instrument", key = "#isin")
    public Instrument getInstrument(String isin) {
        return instrumentRepository.getInstrument(isin);
    }

    /**
     * Add given instrument into cache.
     *
     * @param instrument Instrument instance to add into the cache.
     * @return Instrument added in the cache.
     */
    @CachePut(value = "instrument", key = "#instrument.isin")
    public Instrument addInstrument(Instrument instrument) {
        return instrumentRepository.addInstrument(instrument);
    }

    /**
     * Delete an instrument by given isin from the cache.
     *
     * @param isin Identifier of the instrument.
     * @return Instrument deleted from the cache.
     */
    @CacheEvict(value = "instrument", key = "#isin")
    public Instrument deleteInstrument(String isin) {
        return instrumentRepository.deleteInstrument(isin);
    }

}
