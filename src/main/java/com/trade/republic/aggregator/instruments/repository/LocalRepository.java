/*
 * Copyright (C) 2021 Iván Camilo Sanabria.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.trade.republic.aggregator.instruments.repository;

import com.trade.republic.aggregator.instruments.model.Instrument;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Class responsible of handling persistence against cache.
 *
 * @author Iván Camilo Sanabria (icsanabriar@googlemail.com)
 * @since  1.0.0
 */
@Component
public class LocalRepository implements InstrumentRepository {

    /**
     * Define storage for local repository.
     */
    private final ConcurrentHashMap<String, Instrument> cache = new ConcurrentHashMap<>();

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Instrument> getInstruments() {
        return new ArrayList<>(cache.values());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Instrument getInstrument(String isin) {
        if (null == isin || isin.isEmpty())
            return null;
        return cache.get(isin);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Instrument addInstrument(Instrument instrument) {
        final String isin = instrument.getIsin();
        if (null == isin || isin.isEmpty())
            return null;
        return cache.put(isin, instrument);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Instrument deleteInstrument(String isin) {
        if (null == isin || isin.isEmpty())
            return null;
        return cache.remove(isin);
    }

}
