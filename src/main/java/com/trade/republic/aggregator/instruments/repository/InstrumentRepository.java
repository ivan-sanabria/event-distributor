/*
 * Copyright (C) 2021 Iván Camilo Sanabria.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.trade.republic.aggregator.instruments.repository;

import com.trade.republic.aggregator.instruments.model.Instrument;

import java.util.List;

/**
 * Interface responsible of handling operations against cache for instruments.
 *
 * @author Iván Camilo Sanabria (icsanabriar@googlemail.com)
 * @since  1.0.0
 */
public interface InstrumentRepository {

    /**
     * Retrieves all instruments from cache.
     *
     * @return All Instruments found in the cache.
     */
    List<Instrument> getInstruments();

    /**
     * Retrieves an instrument by isin from cache.
     *
     * @param isin Identifier of the instrument.
     * @return Instrument found in the cache.
     */
    Instrument getInstrument(String isin);

    /**
     * Add given instrument into cache.
     *
     * @param instrument Instrument instance to add into the cache.
     * @return Instrument added in the cache.
     */
    Instrument addInstrument(Instrument instrument);

    /**
     * Delete an instrument by given isin from the cache.
     *
     * @param isin Identifier of the instrument.
     * @return Instrument deleted from the cache.
     */
    Instrument deleteInstrument(String isin);

}
