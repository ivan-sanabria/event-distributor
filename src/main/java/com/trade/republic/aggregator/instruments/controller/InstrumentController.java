/*
 * Copyright (C) 2021 Iván Camilo Sanabria.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.trade.republic.aggregator.instruments.controller;

import com.trade.republic.aggregator.instruments.model.Instrument;
import com.trade.republic.aggregator.instruments.service.InstrumentService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Class responsible of handling request made to /instruments endpoint supporting:
 * <p>
 * - retrieves all the instruments stored in the cache <p>
 * - retrieves the instruments stored in the cache     <p>
 *
 * @author Iván Camilo Sanabria (icsanabriar@googlemail.com)
 * @since  1.0.0
 */
@RestController
@Tag(name = "Instruments Endpoints", description = "Supported operations to query instruments data.")
public class InstrumentController {

    /**
     * Define instrument service to expose instruments on REST api.
     */
    private final InstrumentService instrumentService;

    /**
     * Constructor of the instrument controller to inject required services.
     *
     * @param instrumentService Instrument service used to get instrument data.
     */
    public InstrumentController(InstrumentService instrumentService) {
        this.instrumentService = instrumentService;
    }

    /**
     * Method to handle get request to /instruments/ endpoint.
     *
     * @return Response entity with the list of instrument data.
     */
    @Operation(summary = "Find all instruments given by the partners.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Successfully found the instrument with the given isin on the system.",
                    content = @Content(array = @ArraySchema(schema = @Schema(implementation = Instrument.class)))),
            @ApiResponse(responseCode = "500",
                    description = "Server error occurs by processing the request.",
                    content = @Content)
    })
    @GetMapping(value = "/instruments", produces = "application/json")
    public ResponseEntity<List<Instrument>> getInstruments() {

        final List<Instrument> instruments = instrumentService.getInstruments();
        return new ResponseEntity<>(instruments, HttpStatus.OK);
    }

    /**
     * Method to handle get request to /instruments/isin endpoint.
     *
     * @param isin International Securities Identification Number.
     * @return Response entity with the instrument data, otherwise NOT_FOUND is returned.
     */
    @Operation(summary = "Find specific instrument for a given isin.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Successfully found the instrument with the given isin on the system.",
                    content = @Content(schema = @Schema(implementation = Instrument.class))),
            @ApiResponse(responseCode = "404",
                    description = "An instrument with the given isin was not found.",
                    content = @Content),
            @ApiResponse(responseCode = "500",
                    description = "Server error occurs by processing the request.",
                    content = @Content)
    })
    @GetMapping(value = "/instruments/{isin}", produces = "application/json")
    public ResponseEntity<Instrument> getInstrumentByIsin(@PathVariable String isin) {

        final var instrument = instrumentService.getInstrument(isin);

        if (null == instrument)
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);

        return new ResponseEntity<>(instrument, HttpStatus.OK);
    }

}
