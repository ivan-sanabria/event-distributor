/*
 * Copyright (C) 2021 Iván Camilo Sanabria.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.trade.republic.aggregator.instruments.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serializable;

/**
 * Class to encapsulate the data stored in cache.
 *
 * @author Iván Camilo Sanabria (icsanabriar@googlemail.com)
 * @since  1.0.0
 */
@Data
@AllArgsConstructor
@ApiModel(description = "Instrument data available in REST api.")
public class Instrument implements Serializable {

    /**
     * Description of the instrument.
     */
    @ApiModelProperty(notes = "Description of the instrument.",
            example = "auctor eruditi definitiones")
    private String description;

    /**
     * International Securities Identification Number.
     */
    @ApiModelProperty(notes = "International Securities Identification Number.",
            example = "TI0774053X34")
    private String isin;

    /**
     * Last price of the instrument.
     */
    @ApiModelProperty(notes = "Last price of the instrument.",
            example = "20.306")
    private Double price;

}