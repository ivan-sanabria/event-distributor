/*
 * Copyright (C) 2021 Iván Camilo Sanabria.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.trade.republic.aggregator.instruments.websocket;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.websocket.ClientEndpoint;
import javax.websocket.ContainerProvider;
import javax.websocket.OnMessage;
import java.net.URI;

/**
 * Websocket client implementation to connect to partners component.
 *
 * @author Iván Camilo Sanabria (icsanabriar@googlemail.com)
 * @since  1.0.0
 */
@ClientEndpoint
public class WSClient {

    /**
     * Define logger instance to track errors.
     */
    private static final Logger LOG = LoggerFactory.getLogger(WSClient.class);

    /**
     * Define message handler to process instrument or quote events.
     */
    private MessageHandler messageHandler;

    /**
     * Constructor of the websocket to connect to given endpoint.
     *
     * @param endpoint Endpoint to connect.
     */
    WSClient(URI endpoint) {

        try {

            final var container = ContainerProvider.getWebSocketContainer();
            container.connectToServer(this, endpoint);

        } catch (Exception e) {
            LOG.error("Error connecting to the websocket {}", endpoint, e);
        }
    }

    /**
     * {@inheritDoc}
     */
    @OnMessage
    public void onMessage(String message) {
        if (this.messageHandler != null) {
            this.messageHandler.handleMessage(message);
        }
    }

    /**
     * Define the message handler for the websocket client.
     *
     * @param messageHandler Message handler to process events of the websocket.
     */
    void addMessageHandler(MessageHandler messageHandler) {
        this.messageHandler = messageHandler;
    }

    /**
     * Message handler interface to abstract logic to handle messages.
     */
    public interface MessageHandler {

        /**
         * Function to process message.
         *
         * @param message Message received from websocket.
         */
        void handleMessage(String message);
    }

}
