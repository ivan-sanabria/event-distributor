/*
 * Copyright (C) 2021 Iván Camilo Sanabria.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.trade.republic.aggregator.instruments.websocket;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.trade.republic.aggregator.instruments.model.Instrument;
import com.trade.republic.aggregator.instruments.service.InstrumentService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.net.URI;
import java.net.URISyntaxException;

/**
 * Class responsible of receiving the instrument events and adding / deleting records on cache.
 *
 * @author Iván Camilo Sanabria (icsanabriar@googlemail.com)
 * @since  1.0.0
 */
@Component
public class InstrumentDistributor {

    /**
     * Define logger instance to track errors.
     */
    private static final Logger LOG = LoggerFactory.getLogger(InstrumentDistributor.class);

    /**
     * Define instrument service to expose instruments on REST api.
     */
    private final InstrumentService instrumentService;

    /**
     * Object Mapper used to write JSON response data into content bodies.
     */
    private ObjectMapper objectMapper;

    /**
     * URL of the partners websocket.
     */
    private String url;

    /**
     * Constructor of the instrument distributor to inject required services.
     *
     * @param instrumentService Instrument service used to store instrument data.
     */
    public InstrumentDistributor(InstrumentService instrumentService,
                                 @Autowired ObjectMapper objectMapper,
                                 @Value("${websocket.url}") String url) {
        this.instrumentService = instrumentService;
        this.objectMapper = objectMapper;
        this.url = url;
        this.listenEvents();
    }

    /**
     * Listen instrument events and stored in cache.
     */
    private void listenEvents() {

        try {

            final var client = new WSClient(new URI(url));

            client.addMessageHandler(message -> {

                try {

                    final var event = objectMapper.readValue(message, InstrumentEvent.class);

                    if (event.getType().equals("DELETE")) {

                        instrumentService.deleteInstrument(event.getData().getIsin());

                    } else {

                        final var instrument = new Instrument(
                                event.getData().getDescription(),
                                event.getData().getIsin(),
                                null);

                        instrumentService.addInstrument(instrument);
                    }

                } catch (JsonProcessingException je) {
                    LOG.error("Error parsing event",je);
                }
            });

           Thread.sleep(3000);

        } catch (URISyntaxException ue) {
            LOG.error("Error parsing given url {}", url, ue);
        } catch (InterruptedException ie) {
            LOG.error("Error waiting to load some instruments {}", url, ie);
            Thread.currentThread().interrupt();
        }
    }

}
